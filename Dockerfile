FROM rust:1.47 as builder
COPY . .
RUN cargo build --release

FROM debian:buster-slim
COPY --from=builder target/release/pronouner /usr/local/bin/
VOLUME /data

CMD pronouner -c /data/config.yaml
