# Pronouner #

Pronouner is a Discord bot for managing pronouns on your Discord server.  Using
the bot, users can choose roles that exactly describe their pronouns, all
without needing to use an absurd number of roles.

By default the bot contains a small set of nine common pronouns, although you
can easily change this to add or remove any pronouns as you see fit.  However,
don't worry about adding every possible pronouns, since users themselves can add
server-specific pronouns using the `!pronouns create` command, which allows them
to enter any three-form pronoun and make it available for anyone on that server,
with or without a modifier.

Modifiers are a single word that precedes a pronoun indicating how the user
wants it to be used.  For example, someone might set their pronouns to `Trying
She/Her/Hers` to indicate that they are experimenting with that role.

Pronoun roles can also be combined to better describe your pronouns.  For
example, someone who uses any pronouns except for he or she could add `Any
Pronouns` followed by `Except He/Him/His` and `Except She/Her/Hers`

You might think that all of these possible pronouns would add up to be a lot of
roles, but the bot is able to clean up any roles that aren't actively being
used.  This means that even large servers, or servers with many pronouns can
make sure that every member can exactly describe their pronouns without needing
to use all their roles.

Already using a different system for your pronouns and don't want to put in the
effort to migrate?  The bot comes with a tool to help you with your transition
(hew hew).  Typing `!pronouns transition` (requires the *Manage Roles*
permission) automatically converts almost any pronoun setup into a format usable
by the bot, all while keeping existing users' pronouns.

## Usage ##

| Command                           | Description                              |
| --------------------------------- | ---------------------------------------- |
| `!pronouns`                       | Prints a list of available pronouns and modifiers, as well as general info on how to use the bot |
| `!pronouns [modifier] <pronouns>` | Adds the named pronouns to the user's pronouns, create a new role if necessary. |
| `!pronouns create`                | Creates a pronoun set that wasn't on the server before.  This is done through a series of DMs, and the user must manually add the role after it has been created using `!pronouns [modifier] <pronouns>` |
| `!pronouns remove [mod] <pronouns>` | Removes a pronoun set from the user's pronouns, and deletes the role, if possible |
| `!pronouns help`                  | Sends the user a very detailed help guide on how to use the bot, as well as an invite link should they want to add the bot to their own server |
| `!pronouns stats`                 | Prints out the exact number of users with each pronoun role.  This only counts roles that have at least one user.|
| `!pronouns prune`                 | Manually force the bot to check all existing pronoun roles to see if any can be safely deleted. |
| `!pronouns transition`            | **[Requires *Manage Roles* Permission]** Converts pre-existing roles to the bot's format.  Please read `!pronouns help` before doing this. |

Additionally, anyone with the ability to *Manage Roles* is able to modify the
available pronouns.  This bot stores no data (except static config files)
locally, meaning just by changing the roles available on your server, you can
affect the bot.

Deleting all of the roles that mention a pronoun deletes that pronoun.  Adding a
role (that matches the required case and format of the bot) adds that pronoun.
Changing the color of a role changes the color of any (newly created) modified
version of that role.  And so on.

## Installation ##

This bot is written in Rust, and published using Docker, so there's several ways you can
go about installing it.  For development purposes, you can clone the repository and run
the bot as an unprivileged user, then quickly recompile if you make changes.  In a
production environment, the Docker image is a safe and easy way to deploy and update the
bot.  For other uses, installation using cargo or by downloading a pre-compiled binary
directly can be convenient.

### Docker ###

Docker images are automatically built and deployed using GitLab CI/CD.  The images are
available from the GitLab container repository at
`registry.gitlab.com/alch_emi/pronouner:version-string`.  The only requirement of the bot
is to mount a configuration volume at /data.  This can be read-only once the config is
generated.

Browse available versions [in the container registry][1]

Installation using this method requires that you've installed and set up Docker.

```bash
docker run --volume /etc/pronouner:/data registry.gitlab.com/alch_emi/pronouner:2.0.2
```

### From Source ###

If you want to play with the code or try out unreleased features, you'll need to download
the source and build from that.  This is also a way to run the bot without being a
superuser, although if that's all you want to do, it may also suit your needs to download
the binary directly.

This requires that you have [cargo][2] installed.

```bash
# Download & enter the repository
git clone https://gitlab.com/Alch_Emi/pronouner.git
cd pronouner

# Some things you might want to do: (can be run independantly)

# Run the tests
cargo test

# Build & run the binary
cargo build --release # release flag is optional, takes longer to build, but faster results
cp target/release/pronouner /usr/bin
pronouner -c your-config.yml

# Run directly (in debug mode)
cargo run
```

### Download the Binary ###

Downloading the binary directly will let you run the bot without needing to install any
special software, as long as you can run x86 binaries built for Linux.  Release downloads
can be found on [our CI/CD pipeline page][3].  To download, find the release you want
(probably the most recent one), and click the download option on the right.  Choose the
option listed as deploy:archive.  Unzip the file, change the permissions to allow
executing it, and run!

### Install With Cargo ###

Cargo also has an option to install the package without needing to go through the process
described in the "build from source" section.  This should work on any system, although
I've only tested it on Linux.  You will need to install [cargo][2] though.

```bash
# Feel free to change the tag to the most recent version, or just leave off the --tag
# option to try unreleased code
cargo install --git https://gitlab.com/Alch_Emi/pronouner.git --tag 2.0.2
pronouner -c my-config.yml
```

## Configuration ##

Before running the bot, you need to very briefly configure it.  While it is
possible to run the bot with the default options by passing in the `-t |
--token` argument, this leaves you with less configuration choices (and requires
that you pass a token on the command line, which is often considered bad
practice).

To generate the inital config file, run the bot with the `-c | --config`
argument set to where you want the config file to generate.  If all goes well,
you should get a message indicating that the file has been generated.  Next,
open that up with your editor to see the available configuration options.

### Token ###
This is the only option that isn't filled out by default.  This
should  be your Discord bot token, which you can find in the "Bot" tab of the
Discord Developer Portal's page for your application.  Alternatively, you can
leave this as it is and pass in the token on the command line

### Modifiers ###
This is a list of available modifiers for your pronouns.  Users
will not be able to use modifiers outside the ones listed here.  For each
available modifier, there should be one feild with the name of that modifier.
Under that feild is a list of (case-insensitive) synonyms for that modifier.
For example,

```yaml
    Trying:
      - maybe
      - testing
```

would yeild a modifier called "Trying", with two synonyms, "maybe" and
"testing".  That means that if someone were to type `!pronouns maybe she`, she
would receive the role "Trying She/Her/Hers".  If you don't want a pronoun to
have synonyms, denote this with an empty array, like this: `Trying: []`.

### Special Pronouns ###
This is a list of pronouns that do not conform to the standard three form
syntax.  These pronouns will be available permanently on all servers.  There is
no other way for users to have pronouns that don't conform to the three-form
syntax, so make sure this list is all-inclusive.  For example, adding `Any
Pronouns` to the list ensures that users are able to run `!pronouns any
pronouns` in order to receive the "Any Pronouns" role.  These work with
modifiers as well, and otherwise behave exactly like normal pronouns

### Common Pronouns ###
A list of commonly used pronouns.  For the moment, this list is just merged with
the special pronouns list, although they are kept seperate to make a distinction
between pre-entered roles and pronouns that genuinely don't fit the
three-form syntax.

Adding pronouns here instead of in a server provides several benefits:

 - These pronouns are automatically set up on all servers, without users needing
   to go through the `!pronouns create` dialog for each of them.
 - Pronouns listed here don't need a role to be present at all times, so if no
   one is using these pronouns, they take up no extra roles.  (Normally, roles
   with modifiers are pruned, but at least one role from each set needs to
   exist, otherwise the bot forgets them)
 - Pronouns listed here make servers transitioning using the `!transition`
   command go a little smoother, since these roles can be used to recognize
   existing roles and translate them better
 - Pronouns here can't be entered incorrectly by users, so the server admins
   need to do less cleanup work.

### Build Features ###

By default, pronouner comes with limited support for PluralKit users.  Namely, it can
detect when a command is coming from a PluralKit user, and look up system information to
better accommodate them.  This feature can be disabled by adding the
`--no-default-features` option to cargo when building.

[1]: https://gitlab.com/Alch_Emi/pronouner/container_registry/eyJuYW1lIjoiYWxjaF9lbWkvcHJvbm91bmVyIiwidGFnc19wYXRoIjoiL0FsY2hfRW1pL3Byb25vdW5lci9yZWdpc3RyeS9yZXBvc2l0b3J5LzE0MzkyMTAvdGFncz9mb3JtYXQ9anNvbiIsImlkIjoxNDM5MjEwLCJjbGVhbnVwX3BvbGljeV9zdGFydGVkX2F0IjpudWxsfQ==
[2]: https://www.rust-lang.org/tools/install
[3]: https://gitlab.com/Alch_Emi/pronouner/-/pipelines?scope=tags
