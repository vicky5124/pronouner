concat!(
    "This bot is distributed under the AGPL-3.0-or-later, and the source is available at ",
    env!("CARGO_PKG_REPOSITORY")
)