**Command Usage:** `!pronouns remove [modifier] <base>`

If you've added a pronoun that you no longer want, you can remove it using this command.  The syntax is the same as adding a pronoun: if you want to get rid of a role named `{sample_modifier} Per/Per/Perself`, just type `!pronouns remove {sample_modifier} per/per/preself` (or just `!pronouns remove per`).
