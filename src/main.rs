// Copyright 2019 Emi Simpson <emi@alchemi.dev>

// This file is part of the Pronouner project.
//
// The Pronouner project is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// The Pronouner project is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with the Pronouner project.  If not, see <https://www.gnu.org/licenses/>.

use clap::{App, Arg};
use env_logger::Env;
use serde::{Serialize, Deserialize};

use std::collections::HashMap;

use libpronouner::start_bot;

const DEFAULT_TOKEN: &str = "<Put your Discord token here>";

#[derive(Debug, Serialize, Deserialize, Clone)]
struct Config {
    token: String,
    modifiers: HashMap<String, Vec<String>>,
    special_pronouns: Vec<String>,
    common_pronouns: Vec<String>,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            token: DEFAULT_TOKEN.to_string(),
            modifiers: [
                ("Trying".to_string(), vec![
                    "maybe".to_string(),
                    "testing".to_string(),
                    "experimenting".to_string(),
                ]),
                ("Prefer".to_string(), Vec::new()),
                ("Except".to_string(), vec![
                    "not".to_string(),
                    "no".to_string(),
                ]),
            ].iter().cloned().collect(),
            special_pronouns: vec![
                "Any Pronouns".to_string(),
                "No Pronouns".to_string()
            ],
            common_pronouns: vec![
                "She/Her/Hers".to_string(),
                "He/Him/His".to_string(),
                "They/Them/Theirs".to_string(),
                "Xe/Xem/Xyrs".to_string(),
                "Ne/Nem/Nirs".to_string(),
                "E/Em/Eirs".to_string(),
                "Ze/Zir/Zirs".to_string(),
            ],
        }
    }
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let args = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(Arg::with_name("config")
            .short("c")
            .long("config")
            .value_name("FILE")
            .help(
                "A file with which to configure the bot.  Will be generated if \
                missing.")
            .takes_value(true))
        .arg(Arg::with_name("token")
            .short("t")
            .long("token")
            .value_name("TOKEN")
            .help(
                "The bot token received from Discord.  This overwrites the one \
                provided by the config file if provided."))
        .get_matches();

    let mut config: Config =
        if let Some(config_path) = args.value_of("config") {
            confy::load_path(config_path).unwrap()
        } else {
            Config::default()
        };

    config.special_pronouns.append(&mut config.common_pronouns);

    let token = args.value_of("token")
        .map(|t| t.to_string())
        .unwrap_or(config.token);

    if token != DEFAULT_TOKEN {
        start_bot(&token, config.modifiers, config.special_pronouns)
            .await
            .expect("Failed to start bot");
    } else if args.is_present("config") {
        println!(
            "A config has been generated.  Please fill out the token field \
            with a valid Discord bot token, or pass one in with [--token | \
            -t]"
        );
    } else {
        println!(
            "You must either pass in a config with a valid token \
            [--config | -c] or a Discord bot token [--token | -t]"
        );
    }
}
