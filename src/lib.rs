// Copyright 2019 Emi Simpson <emi@alchemi.dev>

// This file is part of the Pronouner project.
//
// The Pronouner project is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// The Pronouner project is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with the Pronouner project.  If not, see <https://www.gnu.org/licenses/>.

//! Welcome to the docs for the Pronouner pronoun bot project.  Check out the
//! [`PronounBot`] struct for an overview of the functions that drive the bot,
//! or the [`pronoun`] module if you want to know about the underlying
//! model that the bot uses.  If you came here looking for information on how to
//! use the bot as a user, you're probably in the wrong place.  Try typing
//! `!pronouns help` in a server with this bot to be sent the user guide, or
//! check out the project's README.md on the projects git repo.  Still got
//! questions?  You're probably not alone.  Head over to the GitLab page and
//! open up an issue!
//!
//! Fun fact!  Excluding examples, this documentation never uses the same
//! example pronouns twice.

use log::info;
use std::collections::HashMap;

pub mod pronoun;
mod bot;
#[cfg(feature = "pluralkit")]
pub mod pluralkit;

pub use bot::PronounBot;

use serenity::{
    Client,
    client::bridge::gateway::GatewayIntents,
    Result,
};

/// Starts up a [`PronounBot`] with the given configuration
///
/// Automatically connects the client, sets up the framework and handlers and
/// all that junk.  This method will not exit or return without panicking or
/// returning an error.
///
/// As with [`PronounBot::new`], the `modifiers` feild links the modifier's
/// proper form with any synonyms of that modifier.
pub async fn start_bot<S: AsRef<str>, H: std::hash::BuildHasher>(
    token: &str,
    modifiers: HashMap<impl ToString, impl IntoIterator<Item=S>, H>,
    specials: Vec<String>
) -> Result<()> {
    info!("Starting client...");
    let pronouner = PronounBot::new(modifiers, specials);
    let mut client = Client::builder(&token)
        .event_handler(pronouner.clone())
        .framework(pronouner)
        .intents(
            GatewayIntents::GUILDS |
            GatewayIntents::GUILD_MEMBERS |
            GatewayIntents::GUILD_MESSAGES |
            GatewayIntents::DIRECT_MESSAGES
        )
        .await?;
    client.start().await?;
    panic!("client.start exited successfully.  This shouldn't happen");
}
