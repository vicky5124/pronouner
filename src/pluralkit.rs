// Copyright 2020 Emi Simpson <emi@alchemi.dev>

// This file is part of the Pronouner project.
//
// The Pronouner project is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// The Pronouner project is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
// for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with the Pronouner project.  If not, see <https://www.gnu.org/licenses/>.

//! This module provides various models and methods for working with the PluralKit API.

use lazy_static::lazy_static;

use chrono::{DateTime, Utc};

use futures::TryFutureExt;

use url::Url;

use serenity::model::id::{
    MessageId,
    UserId,
    ChannelId,
};

use serde::{Serialize, Deserialize};

use std::{
    error,
    fmt,
    fmt::Display,
};

lazy_static! {
    static ref PLURALKIT_URI_BASE: Url = Url::parse("https://api.pluralkit.me")
        .expect("Invalid Pluralkit API base specified");

    static ref CLIENT: reqwest::Client = reqwest::Client::new();
}

#[derive(Debug)]
#[non_exhaustive]
pub enum PkError {
    NotFound,
    HttpError(reqwest::Error),
    DeserializeError(serde_json::error::Error),
}

impl Display for PkError {
   fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
       match self {
           Self::NotFound =>
               write!(f, "Resource not found in PK's servers"),
           Self::HttpError(e) =>
               write!(f, "An HTTP error occured while connecting to PK's servers: {}", e),
           Self::DeserializeError(e) =>
               write!(f, "Problem occured interpretting response from the PK API: {}", e),
       }
   }
}

impl error::Error for PkError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            Self::HttpError(e) => Some(e),
            Self::DeserializeError(e) => Some(e),
            _ => None,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum PkPrivacySetting {
    Private,
    #[serde(other)]
    Public,
}

impl Default for PkPrivacySetting {
    fn default() -> Self {
        Self::Public
    }
}

/// A five character identifier for a [`PkMember`].
///
/// Each member has a globally unique id, which is used by the API to identify and query
/// system members.
#[derive(Serialize, Deserialize, Debug)]
#[serde(transparent)]
pub struct PkMemberId(String);

/// A five character identifier for a [`PkSystem`].
///
/// Each system has a globally unique id, which is used by the API to identify and query
/// systems.  Make sure that you don't confuse this with the u64 id that Discord assigns
/// to each User, which is differnt.  Discord IDs and PK IDs do correspond 1:1, but
/// different IDs are used for different APIs.
#[derive(Serialize, Deserialize, Debug)]
#[serde(transparent)]
pub struct PkSystemId(String);

/// The pattern used to identify which [`PkMember`] sent a message.
///
/// For example, if the member Lily had a proxy tag with the prefix '[' and the suffix
/// ']', then a message sent by Discord account Lily's system uses that looked like this:
/// "[hey everyone <3]" would be interpretted as a message from Lily reading "hey everyone
/// <3", and be proxied as such.
#[derive(Serialize, Deserialize, Debug)]
pub struct ProxyTag {
    prefix: Option<String>,
    suffix: Option<String>,
}

/// Represents a proxied message sent through pk by a system.
///
/// This contains information on the original context of the message, as well as the user
/// account which triggered it.
#[derive(Serialize, Deserialize, Debug)]
pub struct PkMessage {
    pub timestamp: DateTime<Utc>,
    pub id: MessageId,
    pub original: MessageId,
    pub sender: UserId,
    pub channel: ChannelId,
    pub system: PkSystem,
    pub member: PkMember,
}

/// Represents a system of headmates in PK
///
/// As a brief refresher, a system is a group of headmates who share one body (and also
/// one Discord account).  Each system is comprised of one or more [members][PkMember],
/// each of which is an individual.  PK allows a system to use a single Discord account,
/// and have members represented through webhooks.
#[derive(Serialize, Deserialize, Debug)]
pub struct PkSystem {
    pub id: PkSystemId,
    pub name: Option<String>,
    pub description: Option<String>,
    pub tag: Option<String>,
    pub avatar_url: Option<Url>,
    pub tz: Option<String>,
    pub created: DateTime<Utc>,
    pub description_privacy: Option<PkPrivacySetting>,
    pub member_list_privacy: Option<PkPrivacySetting>,
    pub front_privacy: Option<PkPrivacySetting>,
    pub front_history_privacy: Option<PkPrivacySetting>,
}

/// Represents a member of a [`PkSystem`]
///
/// Members are single users of PluralKit, but exist as a sort of "virtual user"
/// controlled by a single Discord account, which is shared by a group of one or more
/// members, called a system.
#[derive(Serialize, Deserialize, Debug)]
pub struct PkMember {
    pub id: PkMemberId,
    pub name: Option<String>,
    pub display_name: Option<String>,
    pub description: Option<String>,
    pub pronouns: Option<String>,
    pub color: Option<String>,
    pub avatar_url: Option<Url>,
    pub birthday: Option<String>,
    pub proxy_tags: Vec<ProxyTag>,
    pub keep_proxy: bool,
    pub created: DateTime<Utc>,
    pub visibility: Option<PkPrivacySetting>,
    pub name_privacy: Option<PkPrivacySetting>,
    pub description_privacy: Option<PkPrivacySetting>,
    pub avatar_privacy: Option<PkPrivacySetting>,
    pub birthday_privacy: Option<PkPrivacySetting>,
    pub pronoun_privacy: Option<PkPrivacySetting>,
    pub metadata_privacy: Option<PkPrivacySetting>,
}

impl PkMessage {

    /// Query the PK API to retrieve information about a message.
    ///
    /// Creates a [`PkMessage`] representation of a given message.  Keep in mind that this
    /// makes a call to an external API.  Returns [`PkError::NotFound`] if the message was
    /// not sent by a PK user.  Returns [`PkError::HttpError`] if the request fails.
    ///
    /// Note that because this method relies on external APIs, it's a good idea to check
    /// if the message ID provided is a webhook before passing it, in order to save on API
    /// calls and time
    pub async fn query_message(message_id: MessageId) -> Result<Self,PkError> {
        let endpoint = PLURALKIT_URI_BASE.join("/v1/msg/")
            .expect("Configured with invalid PK API endpoint")
            .join(&message_id.as_u64().to_string())
            .expect("Problem with provided message id?  IDK come look");

        let response = CLIENT.get(&endpoint.to_string())
              .send()
              .and_then(reqwest::Response::text)
              .map_err(PkError::HttpError)
              .await?;

        serde_json::from_str(&response)
            .map_err(PkError::DeserializeError)
    }
}
