# Contributing #

Welcome!

Thanks so much for your time!  This project is currently happily
accepting any contributions that may come our way, although bear in mind that
this project is small and developed by a single student, so updates and
responses may be few and far between.  That said, if you're interested in
contributing, please feel free to pick up an issue and put in a PR.  You're
welcome to go straight to a PR, although I can't guarantee it'll be accepted.

This repository is structured into three main branches:  `major`, `minor`, and
`patch`, which correspond to the three types of releases according to semver,
and the changes commited to them should match up with it as well.  For example,
if you have a change that can't be released in a patch (i.e. adding
functionality), but doesn't need to be released with a major version (i.e.
backwards compatable with previous version), then it should go in `minor`.

When a release is made, all lower branches are merged into the branch being
released, then the version is bumped and a release made, and finally merged into
the upper branches.  But, on any commit that isn't a release, the version string
should end in `-dev`, so a version bump is applied to every branch after.

If you have any question, or want to talk, feel free to open an issue or contact
the maintainer of the repository on Discord, XMPP, or by email.
